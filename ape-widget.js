const widget = new ListWidget();

widget.backgroundColor = new Color('#121212');
widget.setPadding(0, 0, 0, 0);

widget.url = 'https://info.uniswap.org/#/tokens/0x4d224452801ACEd8B2F0aebE155379bb5D594381';

const titleStack = widget.addStack();
const authorStack = widget.addStack();

authorStack.setPadding(0, 12, 0, 0);
titleStack.setPadding(12, 12, 0, 18);

titleStack.layoutHorizontally();
authorStack.layoutHorizontally();

const apeL = await loadImage('https://arweave.net/ZiQx9vTPhhigpdq3r8GQ6IHe_Dz9kb64HElAh-FwzCU');



const headerText = titleStack.addText("Ape");
const coinImm = titleStack.addImage(apeL);

coinImm.imageSize = new Size(7, 7);
headerText.font = new Font('Helvetica-Bold', 18);

const headerTextnxt = titleStack.addText(" Widget");

headerTextnxt.font = new Font('Helvetica-Bold', 18);
headerText.leftAlignText();

const authorText = authorStack.addText("by secondstate.xyz");
authorText.font = new Font('Helvetica-Bold', 8);
authorText.leftAlignText();

headerText.textColor = new Color('#FFFFFF');
headerTextnxt.textColor = new Color('#FFFFFF');
authorText.textColor = new Color('#FFFFFF');

async function createWidget() {
    const apeImage = await loadImage('https://arweave.net/2f84QvBfLrTaQdx9loNnD3TKZ295UTFKe0pZ_T-09gQ');
    const ethereumImage = await loadImage('https://arweave.net/yBMb6G6FzMK5Al9_a4xObX_h4yE6YJRA-QBjdQF49Ms');
    

    const UniPrices = await getUniswapPrice();
    const trend = await getTrend();
  
    const rawApePrice = (UniPrices.rawprice).toString().substring(0, 6);
    const ApePrice = UniPrices.apeprice.toFixed(2);
    const EthPrice = parseInt(UniPrices.ethprice, 10).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  
    
    addApe(apeImage, ` $ ${ApePrice}`, trend.apetrend);
    addEth(ethereumImage, ` $ ${EthPrice}`, trend.ethtrend);
addRawApe(apeImage, ` Ξ ${rawApePrice}`, trend.apetrend);
}

function addApe(apeimage, apeprice, apetrend) {

   const apeStack = widget.addStack();
   apeStack.setPadding(0, 6, 6, 0); 
   apeStack.layoutHorizontally();
  
   const APEimgStack = apeStack.addStack(); 
   const apePriceStack = apeStack.addStack(); 
  
   APEimgStack.setPadding(8, 0, 0, 12);
   apePriceStack.setPadding(12, 0, 6, 18);
  
   const apeImgX = APEimgStack.addImage(apeimage);
   apeImgX.imageSize = new Size(28, 28);
   apeImgX.leftAlignImage();
  
   const priceText = apePriceStack.addText(apeprice);
   priceText.font = new Font('SF Pro', 18);
   apePriceStack.centerAlignContent()

  
  if (apetrend) {
    priceText.textColor = new Color('#FFFFFF'); 
  } else {
    priceText.textColor = new Color('#FA124F');
  }
}

function addEth(ethimage, ethprice, ethtrend) {

   const ethStack = widget.addStack();
   ethStack.setPadding(0, 6, 6, 0);
   ethStack.layoutHorizontally();
  
   const ETHimgStack = ethStack.addStack(); 
   const ethPriceStack = ethStack.addStack(); 
  
   ETHimgStack.setPadding(4, 0, 0, 12);
   ethPriceStack.setPadding(8, 0, 6, 18);
  
   const ethImgX = ETHimgStack.addImage(ethimage);
   ethImgX.imageSize = new Size(28, 28);
   ethImgX.leftAlignImage();
  
   const priceText = ethPriceStack.addText(ethprice);
   priceText.font = new Font('Semi', 18);
   

  
  if (ethtrend) {
    priceText.textColor = new Color('#FFFFFF'); 
  } else {
    priceText.textColor = new Color('#FA124F');
  }
}

function addRawApe(apeimage, apeprice, apetrend) {

  const apeStack = widget.addStack();
  apeStack.setPadding(0, 6, 6, 0); 
  apeStack.layoutHorizontally();
 
  const APEimgStack = apeStack.addStack(); 
  const apePriceStack = apeStack.addStack(); 
 
  APEimgStack.setPadding(4, 0, 6, 12);
  apePriceStack.setPadding(8, 0, 6, 0);
 
  const apeImgX = APEimgStack.addImage(apeimage);
  apeImgX.imageSize = new Size(28, 28);
  apeImgX.leftAlignImage();
 
  const priceText = apePriceStack.addText(apeprice);
  priceText.font = new Font('Semi', 18);
  apePriceStack.centerAlignContent()

 
 if (apetrend) {
   priceText.textColor = new Color('#FFFFFF'); 
 } else {
   priceText.textColor = new Color('#FA124F');
 }
}




async function getTrend() {
  const ape = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=apecoin';
  const eth = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&ids=ethereum';
  const apereq = new Request(ape)
  const ethreq = new Request(eth)
  const aperes = await apereq.loadJSON()
  const ethres = await ethreq.loadJSON()
  const apetrend = aperes[0].price_change_24h > 0
  const ethtrend = ethres[0].price_change_24h > 0
  return { apetrend, ethtrend }
}

async function getUniswapPrice() {
    const uniswap = "https://api.thegraph.com/subgraphs/name/uniswap/uniswap-v3"
    const request = new Request(uniswap)
    request.method = 'POST';
    request.headers = {
                'Content-Type': 'application/json',
              };

    request.body = JSON.stringify({
                query: `{
                    token(id:"0x4d224452801aced8b2f0aebe155379bb5d594381") {
                        symbol
                        derivedETH
                      
                    }
                    bundle(id: "1") { ethPriceUSD }
                  }
              `}
            );
    const result = await request.loadJSON()
    const rawprice = result.data.token.derivedETH
    const apeprice = result.data.bundle.ethPriceUSD * result.data.token.derivedETH
    const ethprice = result.data.bundle.ethPriceUSD

    return { apeprice, ethprice, rawprice }
}


async function loadImage(imgUrl) {
    const req = new Request(imgUrl)
    return await req.loadImage()
}

await createWidget();

Script.setWidget(widget);
Script.complete();
widget.presentSmall();
