# Ape Widget

Ape Widget for iOS via Scriptable.app
  
 <img src="/ape-widget.jpg" alt="Ape Widget for iOS" width="25%" height="25%" />
 <img src="/ape-red.jpg" alt="Ape Widget for iOS" width="25%" height="25%" />

## Getting started

- Download the [Scriptable.app](https://apps.apple.com/us/app/scriptable/id1405459188)

- Click the + button in the top right corner and paste the contents of [ape-widget.js](https://gitlab.com/secondstatebuilds/ape-widget/-/raw/main/ape-widget.js) by manually copying the whole page into the text field, or click here -> [ape-widget.js-mainpage](https://gitlab.com/secondstatebuilds/ape-widget/-/blob/main/ape-widget.js), then click the "Copy file contents" button. 
Finally, click "Done" in Scriptable.app.

- Long press your home screen, click the + sign in the upper left corner then search for "Scriptable" and click "Add Widget".

- Click the "Select script in widget configurator" text, then select the script you previously created for the "Script" dropdown. Finally, set "When Interacting" to "Run Script" then click away. You should now see a new widget appear on your home screen. :]


## Details

Ape Widget:

- Downtrends are indicated as RED text, while uptrends are indicated as WHITE text.

- Both ASH and ETH rate are pulled from [Uniswap v3](https://info.uniswap.org/#/tokens/0x4d224452801ACEd8B2F0aebE155379bb5D594381), and clicking the widget also takes you to the charts of Uniswap.

- Trends are pulled from [CoinGecko](https://www.coingecko.com/)

- Raw ETH-ASH price.


## Credits

- [Dough Boi](https://twitter.com/DoughBoiNFT) for the UI/UX design assistance with Burn Widget / Burn Panel, which I've then retrofitted for Apes :) 
- ?



## TODO

- Submit app to Scriptable.app store. 
- Possible styling?
- Smaller variation

